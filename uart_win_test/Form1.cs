﻿using System;
using System.Collections;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Data;
using System.Drawing; // Add this using directive for ContentAlignment

namespace TestLed1
{
    public class Form1 : Form
    {
        private ComboBox portComboBox;
        // private TextBox pageNumTextBox;
        private TextBox textTextBox;
        private TextBox fontSizeTextBox;
        private TextBox redTextBox;
        private TextBox greenTextBox;
        private TextBox blueTextBox;
        private ComboBox actionComboBox;
        private TextBox speedTextBox;
        private Button sendButton;
        private Button clearHexDisplayButton; // New button to clear hexDisplay
        private Button connectButton;
        private SerialPort serialPort;
        private RichTextBox hexDisplay;  // Added RichTextBox for displaying hexadecimal data

        private bool isConnected = false;
        private int y_loc = 10;

        private const byte SET_TEXT = 0x21;
        private const byte SAVE_TEXT = 0xA1;

        public Form1()
        {
            Text = "LED Test Program V1.0";

            InitializeComponent1();
        }

        private void InitializeComponent1()
        {
            int lineNum = 0;

            // UI 컨트롤 초기화
            Label portLabel = new Label();
            portLabel.Text = "Select Port:";
            portLabel.Location = new System.Drawing.Point(10, y_loc + (30 * lineNum) + 5);
            Controls.Add(portLabel);

            portComboBox = new ComboBox();
            portComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            portComboBox.Location = new System.Drawing.Point(150, y_loc + (30 * lineNum));
            // portComboBox.Width = 80;
            portComboBox.Size = new System.Drawing.Size(80, 20);
            RefreshAvailablePorts();
            Controls.Add(portComboBox);

            // Connect button setup
            connectButton = new Button();
            connectButton.Text = "Connect";
            connectButton.Location = new System.Drawing.Point(250, y_loc + (30 * lineNum) - 2);
            connectButton.Size = new System.Drawing.Size(100, 23);
            connectButton.Click += ConnectButton_Click;
            UpdateConnectButtonColor(); // Initial color setup
            Controls.Add(connectButton);

            /*
            // page Num
            lineNum++;
            Label pageNumLabel = new Label();
            pageNumLabel.Text = "Page Num(1~5):";
            pageNumLabel.Location = new System.Drawing.Point(10, y_loc + (30 * lineNum) + 5);
            Controls.Add(pageNumLabel);

            pageNumTextBox = new TextBox();
            pageNumTextBox.Text = "1";
            pageNumTextBox.Location = new System.Drawing.Point(150, y_loc + (30 * lineNum));
            pageNumTextBox.Size = new System.Drawing.Size(40, 20);
            Controls.Add(pageNumTextBox);
            */

            // Text
            lineNum++;
            Label textLabel = new Label();
            textLabel.Text = "Text:";
            textLabel.Location = new System.Drawing.Point(10, y_loc + (30 * lineNum) + 5);
            Controls.Add(textLabel);

            textTextBox = new TextBox();
            textTextBox.Text = "위험";
            textTextBox.Location = new System.Drawing.Point(150, y_loc + (30 * lineNum));
            textTextBox.Size = new System.Drawing.Size(200, 20);
            Controls.Add(textTextBox);

            // Font size
            lineNum++;
            Label fontSizeLabel = new Label();
            fontSizeLabel.Text = "Font Size (1~4):";
            fontSizeLabel.Location = new System.Drawing.Point(10, y_loc + (30 * lineNum) + 5);
            Controls.Add(fontSizeLabel);

            fontSizeTextBox = new TextBox();
            fontSizeTextBox.Text = "4";
            fontSizeTextBox.Location = new System.Drawing.Point(150, y_loc + (30 * lineNum));
            fontSizeTextBox.Size = new System.Drawing.Size(40, 20);
            Controls.Add(fontSizeTextBox);

            // Color text boxes setup
            lineNum++;
            Label colorLabel = new Label();
            colorLabel.Text = "TextColor RGB:\n(0 ~ 255)";
            colorLabel.Location = new System.Drawing.Point(10, y_loc + (30 * lineNum));
            Controls.Add(colorLabel);

            redTextBox = new TextBox();
            redTextBox.Text = "255";
            redTextBox.Location = new System.Drawing.Point(150, y_loc + (30 * lineNum));
            redTextBox.Size = new System.Drawing.Size(40, 20);
            Controls.Add(redTextBox);

            greenTextBox = new TextBox();
            greenTextBox.Text = "0";
            greenTextBox.Location = new System.Drawing.Point(200, y_loc + (30 * lineNum));
            greenTextBox.Size = new System.Drawing.Size(40, 20);
            Controls.Add(greenTextBox);

            blueTextBox = new TextBox();
            blueTextBox.Text = "0";
            blueTextBox.Location = new System.Drawing.Point(250, y_loc + (30 * lineNum));
            blueTextBox.Size = new System.Drawing.Size(40, 20);
            Controls.Add(blueTextBox);

            // Action
            lineNum++;
            Label actionLabel = new Label();
            actionLabel.Text = "Scroll:";
            actionLabel.Location = new System.Drawing.Point(10, y_loc + (30 * lineNum) + 5);
            Controls.Add(actionLabel);

            actionComboBox = new ComboBox();
            actionComboBox.Items.AddRange(new object[] { "None", "Right", "Left", "Down", "Up", "Blink" });
            actionComboBox.SelectedIndex = 0;
            actionComboBox.Location = new System.Drawing.Point(150, y_loc + (30 * lineNum));
            actionComboBox.Size = new System.Drawing.Size(80, 20);
            Controls.Add(actionComboBox);

            // Scroll Speed
            lineNum++;
            Label speedLabel = new Label();
            speedLabel.Text = "Scroll Speed:\n(ms)";
            speedLabel.Location = new System.Drawing.Point(10, y_loc + (30 * lineNum) + 5);
            Controls.Add(speedLabel);

            speedTextBox = new TextBox();
            speedTextBox.Text = "50";
            speedTextBox.Location = new System.Drawing.Point(150, y_loc + (30 * lineNum));
            speedTextBox.Size = new System.Drawing.Size(40, 20);
            Controls.Add(speedTextBox);

            // Send Button
            lineNum += 2;
            sendButton = new Button();
            sendButton.Text = "Send";
            sendButton.Location = new System.Drawing.Point(80, y_loc + (30 * lineNum));
            sendButton.Size = new System.Drawing.Size(100, 30);
            sendButton.BackColor = System.Drawing.Color.FromArgb(255, 180, 100); // Light orange color
            sendButton.Click += SendButton_Click;
            Controls.Add(sendButton);

            // Clear Button
            clearHexDisplayButton = new Button();
            clearHexDisplayButton.Text = "Clear";
            clearHexDisplayButton.Location = new System.Drawing.Point(180, y_loc + (30 * lineNum));
            clearHexDisplayButton.Size = new System.Drawing.Size(100, 30);
            clearHexDisplayButton.BackColor = System.Drawing.Color.FromArgb(255, 180, 100); // Light orange color
            clearHexDisplayButton.Click += ClearHexDisplayButton_Click;
            Controls.Add(clearHexDisplayButton);

            // Add RichTextBox for displaying hexadecimal data
            lineNum++;
            hexDisplay = new RichTextBox();
            hexDisplay.Location = new System.Drawing.Point(10, y_loc + (30 * lineNum) + 5);
            hexDisplay.Size = new System.Drawing.Size(340, 600);
            hexDisplay.BackColor = System.Drawing.Color.FromArgb(255, 240, 230); // Light orange color
            hexDisplay.ReadOnly = true;
            // Set the font to Courier New for a fixed-width appearance
            hexDisplay.Font = new System.Drawing.Font("Consolas", 12);
            Controls.Add(hexDisplay);

            // Set the ClientSize property to a larger size
            ClientSize = new System.Drawing.Size(360, 865);
        }

        private void ConnectButton_Click(object sender, EventArgs e)
        {
            string selectedPort = portComboBox.SelectedItem?.ToString();
            if (string.IsNullOrEmpty(selectedPort))
            {
                CenteredMessageBox("Please select a valid serial port.");
                return;
            }

            if (!isConnected)
            {
                // Initialize the SerialPort object
                serialPort = new SerialPort(selectedPort, 115200);

                try
                {
                    // Try to open the serial port
                    serialPort.Open();
                    // CenteredMessageBox($"Connected to {selectedPort} successfully.");

                    // Update the button text and set the flag
                    connectButton.Text = "Connected";
                    isConnected = true;

                    // Update the Connect button color
                    UpdateConnectButtonColor();
                }
                catch (Exception ex)
                {
                    CenteredMessageBox($"Failed to open serial port.\n {ex.Message}");
                }
            }
            else
            {
                // Disconnect
                try
                {
                    // Close the serial port
                    serialPort.Close();
                    // CenteredMessageBox($"Disconnected from {selectedPort}.");

                    // Update the button text and reset the flag
                    connectButton.Text = "Connect Port";
                    isConnected = false;

                    // Update the Connect button color
                    UpdateConnectButtonColor();
                }
                catch (Exception ex)
                {
                    CenteredMessageBox($"Failed to close serial port.\n {ex.Message}");
                }
                finally
                {
                    // Dispose of the serial port
                    serialPort.Dispose();
                }
            }
        }

        private byte GetColorValue(string input)
        {
            if (byte.TryParse(input, out byte value))
            {
                return value;
            }
            else
            {
                CenteredMessageBox($"Invalid color input: {input}.\n Using default value (0).");
                return 0;
            }
        }

        private void DisplayHexData(byte[] data)
        {
            hexDisplay.AppendText($"<<< {data.Length} bytes 전송, Hex형식 >>> \n");
            //int count = 0;
            //foreach (byte b in data)
            //{
            //    hexDisplay.AppendText($"{b:X2} ");
            //    if (++count >= 10)
            //    {
            //        count = 0;
            //        hexDisplay.AppendText("\n");
            //    }
            //}
            //hexDisplay.AppendText(Environment.NewLine);

            hexDisplay.AppendText($"{data[0]:X2}        (STX)\n");
            hexDisplay.AppendText($"{data[1]:X2} {data[2]:X2}     (Data Length 하상)\n");
            hexDisplay.AppendText($"{data[3]:X2} {data[4]:X2}     (Data Length 하상 x2)\n");
            hexDisplay.AppendText($"{data[5]:X2}        (Command)\n"); // 0xA1
            hexDisplay.AppendText($"{data[6]:X2}        (Option)\n"); // option
            hexDisplay.AppendText($"{data[7]:X2}        (Page Num)\n"); // page num
            hexDisplay.AppendText($"{data[8]:X2} {data[9]:X2}     (문자 byte수 상하)\n"); // 문자수 상하

            int i = 10;
            // int count = 0;
            for (; i < 10 + data[9]; i++)
            {
                hexDisplay.AppendText($"{data[i]:X2} ");
                //if (++count >= 10)
                //{
                //    count = 0;
                //    hexDisplay.AppendText("\n");
                //}
            }
            hexDisplay.AppendText($" (문자)\n");
            hexDisplay.AppendText($"{data[i++]:X2}        (Font size)\n"); // font size
            hexDisplay.AppendText($"{data[i++]:X2} {data[i++]:X2} {data[i++]:X2}  (문자색상 RGB)\n"); // 문자 색상
            hexDisplay.AppendText($"{data[i++]:X2} {data[i++]:X2} {data[i++]:X2}  (배경색상 RGB)\n"); // 배경 색상
            hexDisplay.AppendText($"{data[i++]:X2}        (Scroll 방향)\n"); // scroll 방향
            hexDisplay.AppendText($"{data[i++]:X2} {data[i++]:X2}     (Scroll 속도 상하)\n"); // scroll 속도
            hexDisplay.AppendText($"{data[i++]:X2} {data[i++]:X2} {data[i++]:X2} {data[i++]:X2} (예약 영역)\n");
            hexDisplay.AppendText($"{data[i++]:X2} {data[i++]:X2} {data[i++]:X2} {data[i++]:X2} (예약 영역)\n");
            hexDisplay.AppendText($"{data[i++]:X2} {data[i++]:X2}     (CRC16 상하)\n");
            hexDisplay.AppendText($"{data[i++]:X2}        (ETX)");
        }

        private void SendButton_Click(object sender, EventArgs e)
        {
            if (!isConnected)
            {
                CenteredMessageBox("Serial port is not connected.\n Please connect first.");
                return;
            }

            // Clear the hexDisplay before appending new data
            hexDisplay.Clear();

            byte pageNum = 1; // ReadPageNum();
            string text = textTextBox.Text;
            byte fontSize = ReadFontSize();
            byte red = GetColorValue(redTextBox.Text);
            byte green = GetColorValue(greenTextBox.Text);
            byte blue = GetColorValue(blueTextBox.Text);
            byte action = (byte)(actionComboBox.SelectedIndex);
            ushort scrollSpeed = ReadScrollSpeed();

            // 데이터 패킷 생성
            byte[] dataPacket = CreateDataPacket(pageNum, text, fontSize, red, green, blue, action, scrollSpeed);

            // add Header, and transmit
            TransmitDataPacket(SAVE_TEXT, dataPacket);

            // 시리얼 포트를 통해 데이터 전송
            // serialPort.Write(dataPacket, 0, dataPacket.Length);

            // Display the sent data in hexadecimal format
            // DisplayHexData(dataPacket);
        }

        private void ClearHexDisplayButton_Click(object sender, EventArgs e)
        {
            // Clear the hexDisplay
            hexDisplay.Clear();
        }

        private void RefreshAvailablePorts()
        {
            string[] ports = SerialPort.GetPortNames();
            portComboBox.Items.Clear();
            portComboBox.Items.AddRange(ports);
            if (ports.Length > 0)
            {
                portComboBox.SelectedIndex = 0;
            }
        }

        private void UpdateConnectButtonColor()
        {
            // Update Connect button color based on connection state
            connectButton.BackColor = isConnected ? System.Drawing.Color.LightGreen : System.Drawing.Color.FromArgb(255, 240, 230); // Light orange color;
        }

        // Font Size 입력을 읽는 함수
        //private byte ReadPageNum()
        //{
        //    if (byte.TryParse(pageNumTextBox.Text, out byte pageNum) && pageNum >= 1 && pageNum <= 5)
        //    {
        //        return pageNum;
        //    }
        //    else
        //    {
        //        CenteredMessageBox("Invalid scroll speed input.\n Using default speed.");
        //        return 0;
        //    }
        //}

        // Font Size 입력을 읽는 함수
        private byte ReadFontSize()
        {
            if (byte.TryParse(fontSizeTextBox.Text, out byte fontSize) && fontSize >= 1 && fontSize <= 4)
            {
                return fontSize;
            }
            else
            {
                CenteredMessageBox("Invalid scroll speed input.\n Using default speed.");
                return 0;
            }
        }

        // 스크롤 속도 입력을 읽는 함수
        private ushort ReadScrollSpeed()
        {
            if (ushort.TryParse(speedTextBox.Text, out ushort speed) && speed >= 0 && speed <= 65535)
            {
                return speed;
            }
            else
            {
                CenteredMessageBox("Invalid scroll speed input.\n Using default speed.");
                return 0;
            }
        }

        // 데이터 패킷 생성 함수
        private byte[] CreateDataPacket(byte pageNum, string text, byte fontSize, byte red, byte green, byte blue, byte action, ushort scrollSpeed)
        {
            byte[] textBytes = Encoding.UTF8.GetBytes(text);
            int textLength = textBytes.Length;

            List<byte> byteArray = new List<byte>();
            byteArray.Add(0x00);
            byteArray.Add(pageNum);
            byteArray.Add((byte)(textLength >> 8));
            byteArray.Add((byte)textLength);
            byteArray.AddRange(textBytes);
            byteArray.Add(fontSize);
            byteArray.Add(red);
            byteArray.Add(green);
            byteArray.Add(blue);
            byteArray.Add(0x00);
            byteArray.Add(0x00);
            byteArray.Add(0x00);
            byteArray.Add(action);
            byteArray.Add((byte)(scrollSpeed >> 8));
            byteArray.Add((byte)scrollSpeed);
            byteArray.Add(0x00);
            byteArray.Add(0x00);
            byteArray.Add(0x00);
            byteArray.Add(0x00);
            byteArray.Add(0x00);
            byteArray.Add(0x00);
            byteArray.Add(0x00);
            byteArray.Add(0x00);

            byte[] dataPacket = byteArray.ToArray();

            return dataPacket;
        }

        private void TextTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void TransmitDataPacket(byte cmd, byte[] dataPacket)
        {
            // Construct the complete packet with STX, length, dataPacket, and CRC16
            int dataLen = dataPacket.Length + 1;
            byte[] completePacket = new byte[dataLen + 8];
            completePacket[0] = 0x02; // STX
            completePacket[1] = (byte)dataLen; // Length
            completePacket[2] = (byte)(dataLen >> 8);      // Length
            completePacket[3] = completePacket[1];
            completePacket[4] = completePacket[2];
            completePacket[5] = cmd;
            Array.Copy(dataPacket, 0, completePacket, 6, dataPacket.Length);
            ushort crc16 = Crc16_ccitt(completePacket, 5, dataLen);
            completePacket[dataLen + 5] = (byte)(crc16 >> 8); // High byte of CRC16
            completePacket[dataLen + 6] = (byte)crc16; // Low byte of CRC16
            completePacket[dataLen + 7] = 0x03; // ETX

            // Transmit the complete packet to the serial port
            serialPort.Write(completePacket, 0, completePacket.Length);

            // Display the sent complete packet in hexadecimal format
            DisplayHexData(completePacket);
        }

        private ushort Crc16_ccitt(byte[] buf, int startIndex, int len)
        {
            ushort[] crc16tab = {
                0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
                0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef,
                0x1231,0x0210,0x3273,0x2252,0x52b5,0x4294,0x72f7,0x62d6,
                0x9339,0x8318,0xb37b,0xa35a,0xd3bd,0xc39c,0xf3ff,0xe3de,
                0x2462,0x3443,0x0420,0x1401,0x64e6,0x74c7,0x44a4,0x5485,
                0xa56a,0xb54b,0x8528,0x9509,0xe5ee,0xf5cf,0xc5ac,0xd58d,
                0x3653,0x2672,0x1611,0x0630,0x76d7,0x66f6,0x5695,0x46b4,
                0xb75b,0xa77a,0x9719,0x8738,0xf7df,0xe7fe,0xd79d,0xc7bc,
                0x48c4,0x58e5,0x6886,0x78a7,0x0840,0x1861,0x2802,0x3823,
                0xc9cc,0xd9ed,0xe98e,0xf9af,0x8948,0x9969,0xa90a,0xb92b,
                0x5af5,0x4ad4,0x7ab7,0x6a96,0x1a71,0x0a50,0x3a33,0x2a12,
                0xdbfd,0xcbdc,0xfbbf,0xeb9e,0x9b79,0x8b58,0xbb3b,0xab1a,
                0x6ca6,0x7c87,0x4ce4,0x5cc5,0x2c22,0x3c03,0x0c60,0x1c41,
                0xedae,0xfd8f,0xcdec,0xddcd,0xad2a,0xbd0b,0x8d68,0x9d49,
                0x7e97,0x6eb6,0x5ed5,0x4ef4,0x3e13,0x2e32,0x1e51,0x0e70,
                0xff9f,0xefbe,0xdfdd,0xcffc,0xbf1b,0xaf3a,0x9f59,0x8f78,
                0x9188,0x81a9,0xb1ca,0xa1eb,0xd10c,0xc12d,0xf14e,0xe16f,
                0x1080,0x00a1,0x30c2,0x20e3,0x5004,0x4025,0x7046,0x6067,
                0x83b9,0x9398,0xa3fb,0xb3da,0xc33d,0xd31c,0xe37f,0xf35e,
                0x02b1,0x1290,0x22f3,0x32d2,0x4235,0x5214,0x6277,0x7256,
                0xb5ea,0xa5cb,0x95a8,0x8589,0xf56e,0xe54f,0xd52c,0xc50d,
                0x34e2,0x24c3,0x14a0,0x0481,0x7466,0x6447,0x5424,0x4405,
                0xa7db,0xb7fa,0x8799,0x97b8,0xe75f,0xf77e,0xc71d,0xd73c,
                0x26d3,0x36f2,0x0691,0x16b0,0x6657,0x7676,0x4615,0x5634,
                0xd94c,0xc96d,0xf90e,0xe92f,0x99c8,0x89e9,0xb98a,0xa9ab,
                0x5844,0x4865,0x7806,0x6827,0x18c0,0x08e1,0x3882,0x28a3,
                0xcb7d,0xdb5c,0xeb3f,0xfb1e,0x8bf9,0x9bd8,0xabbb,0xbb9a,
                0x4a75,0x5a54,0x6a37,0x7a16,0x0af1,0x1ad0,0x2ab3,0x3a92,
                0xfd2e,0xed0f,0xdd6c,0xcd4d,0xbdaa,0xad8b,0x9de8,0x8dc9,
                0x7c26,0x6c07,0x5c64,0x4c45,0x3ca2,0x2c83,0x1ce0,0x0cc1,
                0xef1f,0xff3e,0xcf5d,0xdf7c,0xaf9b,0xbfba,0x8fd9,0x9ff8,
                0x6e17,0x7e36,0x4e55,0x5e74,0x2e93,0x3eb2,0x0ed1,0x1ef0
            };
            ushort crc = 0;

            for (int i = startIndex; i < startIndex + len; i++)
            {
                crc = (ushort)((crc << 8) ^ crc16tab[(crc >> 8) ^ buf[i]]);
            }

            return crc;
        }

        private void CenteredMessageBox(string message)
        {
            // Create a new form for the custom MessageBox
            Form messageBoxForm = new Form
            {
                Text = "알림",
                Size = new System.Drawing.Size(300, 150),
                FormBorderStyle = FormBorderStyle.FixedDialog,
                StartPosition = FormStartPosition.Manual
            };

            // Calculate the center position based on the main form's location and size
            int centerX = Location.X + Width / 2 - messageBoxForm.Width / 2;
            int centerY = Location.Y + Height / 2 - messageBoxForm.Height / 2;

            // Set the position of the custom MessageBox
            messageBoxForm.Location = new System.Drawing.Point(centerX, centerY);

            // Create a label with the message
            Label label = new Label
            {
                Text = message,
                Dock = DockStyle.Fill,
                TextAlign = ContentAlignment.MiddleCenter // Use ContentAlignment from System.Drawing
            };

            // Add the label to the custom MessageBox form
            messageBoxForm.Controls.Add(label);

            // Show the custom MessageBox as a dialog
            messageBoxForm.ShowDialog();
        }
    }
}
